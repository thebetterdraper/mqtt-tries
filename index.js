var mqtt = require('mqtt')
var express = require('express')
var app = express();

var client = mqtt.connect('https://3.6.150.94')
var topic;
var callbackTopic;
var videoAddresses = [
    "/home/pi/videos/vid4.mp4",
    "/home/pi/videos/vid3.mp4"
]
var queueCallback = "video/queue/callback/";


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.listen(3000, () => {
    console.log("Server running on port 3000");
});

app.get("/video/play/publish", (req, res, next) => {
    topic = "video/play/";
    publisher(topic);

    var response = "check";


    client.on('message', function(topic, message) {
        // message is Buffer
        console.log(message.toString() + "is message");
        response = message.toString();

        console.log(topic + "is topic");

    })

    res.send(response);

});


app.get("/video/play/subscribe", (req, res, next) => {
    topic = "video/play/";
    subscriber(topic);
    res.send(topic);
});


app.get("/video/stop/publish", (req, res, next) => {
    topic = "/video/stop/";
    publisher(topic);
    res.send(topic);

    client.on('message', function(topic, message) {
        // message is Buffer
        console.log(message.toString() + "is message");
        res.send(message.toString());
        console.log(topic + "is topic");

        client.end()
    })
});

app.get("/video/stop/subscribe", (req, res, next) => {
    topic = "video/stop/";
    subscriber(topic);
    res.send(topic);
});


app.get("/video/queue/publish", (req, res, next) => {
    topic = "/video/queue/";
    publisher(topic);
    res.send(topic);

    client.on('message', function(topic, message) {
        // message is Buffer
        console.log(message.toString() + "is message");
        res.send(message.toString());
        console.log(topic + "is topic");

        client.end()
    })
});

app.get("/video/queue/subscribe", (req, res, next) => {
    topic = "/video/queue/#";
    subscriber(topic);
    res.send(topic);
});



// client.on('connect', function() {
//     client.subscribe('presence', function(err) {
//         if (!err) {
//             client.publish('presence', 'Hello mqtt')
//         }
//     })
// })

// client.on('connect', function() {
//     if (client.connected == true) {
//         client.publish("video/play/", "Shinzhou Sasagayo");
//     }
// })


// client.on('connect', function() {
//     client.subscribe(topic);
//     publisher();

// })




function publisher(topic1) {
    console.log("Connecting");


    if (client.connected == true) {
        console.log("Connection made")
        client.publish(topic1, "Shinzhou Sasagayo");
    }
}


function subscriber(topic1) {
    console.log("Subscribing");


    if (client.connected == true) {
        console.log("Subscription made")
        client.subscribe(topic1);
    }
}

// client.on('message', function(topic, message) {
//     // message is Buffer
//     console.log(message.toString() + "is message");
//     console.log(topic + "is topic");

//     client.end()
// })