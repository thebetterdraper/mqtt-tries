var mqtt = require('mqtt')
var express = require('express')
var app = express();

var client = mqtt.connect('https://3.6.150.94')
var server = app.listen(4000);
var io = require('socket.io').listen(server);




// http.listen(3000, () => {
//     console.log('listening on *:3000');
// });

var topic;
var callbackTopic;
// var videoAddresses = [
//     "/home/pi/videos/vid4.mp4",
//     "/home/pi/videos/vid3.mp4"
// ];
var videoAddresses = [{
        "path": "/home/pi/videos/vid4.mp4",
        "duration": 10.7
    },
    {
        "path": "/home/pi/videos/vid4.mp4",
        "duration": 10.7
    }
];
var options = {
    retain: true,
    qos: 1
}


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// app.listen(4000, () => {
//     console.log("Server running on port 4000");
// });


client.on('connect', function() {
    if (client.connected == true) {
        callbackTopic = "+/+/callback/";
        subscriber(callbackTopic);
    }
})


client.on('message', function(callbackTopic, message) {

    // message is Buffer
    var msg = message.toString();
    console.log(msg + "is message");
    // io.on('connection', (socket) => {

    //     io.emit('chat message', msg);
    //     console.log("sent");

    // });



    io.emit('chat message', msg);
    console.log("sent");



    console.log(callbackTopic + "is topic");


    //client.end()
})
app.get("/video/queue/publish", (req, res, next) => {

    topic = "video/queue/";


    publisher(topic, videoAddresses, options);


    // client.on('message', function(callbackTopic, message) {

    //     // message is Buffer
    //     var msg = message.toString();
    //     console.log(msg + "is message");
    //     if (msg === "Created Queue") {
    //         res.write("Created Queue");
    //         //res.end();
    //         count += 1;
    //         console.log("Count is " + count);

    //         console.log("CHECKFLAG");
    //     }

    //     console.log(callbackTopic + "is topic");


    //     //client.end()
    // })
});


function publisher(topic, payload, options) {
    console.log("Connecting");

    payload = JSON.stringify(payload);
    if (client.connected == true) {
        console.log("Connection made")
        client.publish(topic, payload, options);
    }
}

function subscriber(topic) {
    console.log("Subscribing");


    if (client.connected == true) {
        console.log("Subscription made")
        client.subscribe(topic);
    }
}