var jq = $.noConflict();
jq(document).ready(function() {

    const socket = io('http://localhost:4000');
    socket.on('chat message', function(msg1) {
        console.log(msg1);
    });

    async function getter(url) {
        await jq.get(url, function(data, status) {
            alert("Data: " + data + "\nStatus: " + status);

        });
    }

    jq("#btn1").click(function() {
            getter("http://localhost:4000/video/play/subscribe");

        }

    );


    jq("#btn2").click(function() {
            getter("http://localhost:3000/video/queue/subscribe");

        }

    );


    jq("#btn3").click(function() {
            getter("http://localhost:3000/video/stop/subscribe");

        }

    );

    jq("#btn4").click(function() {
            getter("http://localhost:3000/video/play/publish");

        }

    );


    jq("#btn5").click(function() {
            getter("http://localhost:3000/video/queue/publish");

        }

    );


    jq("#btn6").click(function() {
            getter("http://localhost:3000/video/stop/publish");

        }

    );



})