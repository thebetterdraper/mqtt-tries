var mqtt = require('mqtt')
var express = require('express')
var app = express();

var client = mqtt.connect('https://3.6.150.94')
var server = app.listen(4000);
var io = require('socket.io').listen(server);


var topic;
var callbackTopic;

var videoAddresses = [{
        "path": "/home/pi/videos/vid4.mp4",
        "duration": 10.7
    },
    {
        "path": "/home/pi/videos/vid4.mp4",
        "duration": 10.7
    }
];
var options = {
    retain: true,
    qos: 1
}


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});




client.on('connect', function() {
    if (client.connected == true) {
        callbackTopic = "+/+/callback/";
        subscriber(callbackTopic);
    }
})


client.on('message', function(callbackTopic, message) {

    var msg = message.toString();
    console.log(msg + "is message");
    io.emit('chat message', msg);

    console.log(callbackTopic + "is topic");

})
app.get("/video/queue/publish", (req, res, next) => {

    topic = "video/queue/";


    publisher(topic, videoAddresses, options);

});


function publisher(topic, payload, options) {
    console.log("Connecting");

    payload = JSON.stringify(payload);
    if (client.connected == true) {
        console.log("Connection made")
        client.publish(topic, payload, options);
    }
}

function subscriber(topic) {
    console.log("Subscribing");


    if (client.connected == true) {
        console.log("Subscription made")
        client.subscribe(topic);
    }
}